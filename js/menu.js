
function myFunction(x) {
    x.classList.toggle("change");
    document.getElementById("menu-principal").classList.toggle("change-menu-principal");
    document.getElementById("logo-toggle").classList.toggle("change-logo-toggle");
    document.getElementById("items-menu").classList.toggle("change-items-menu");
    document.getElementById("logo-menu").classList.toggle("change-logo-menu");
}  

$(document).ready(function(){

  // Menu Size
  // 
  var menuBar   = $(".menu-bar"),
      container = $(".container-geral");


  function menuSize() {

    var menuBarW     = menuBar.width(),
        windowW      = $( window ).width(),
        menuBarNewW  = (menuBarW*100)/windowW,
        containerNewW= 100 - menuBarNewW;

        
        if ( windowW > 768){
          container.css({"width": containerNewW + "%"});
        } else {
          container.css({"width": "100%"});
        }
        

        console.log(containerNewW);
  }

  $( window ).resize("resize", menuSize);

  menuSize(); //INIT 

  // END Menu Size
  // 
  // 

	$( ".botao-menu" ).click(function() {
	  $( this ).toggleClass( "change-botao-menu" );
    $( ".menu-mobile" ).toggleClass( "change-menu-mobile" );
    
      $( ".div1-home" ).click(function() {

        if ($(".menu-mobile").hasClass("change-menu-mobile")){
          $( ".menu-mobile" ).toggleClass( "change-menu-mobile" );
        }
      });

  });

  // ALTERACAO DE BG COM CLICK NO MENU DESK
  $("#menu-btn").click(function(){
    document.getElementById("menu-principal").classList.toggle("change-menu-principal-ativo");
    
    if ($('#menu-principal').hasClass( "change-menu-principal-ativo" )) {
      $("#mask").removeClass("mask-off");
      $("#mask").addClass("mask");
      if($("#mask").hasClass("mask")){
        // adiciona a segunda classe com delay para uma transicao mais leve
        setTimeout(function() {
          $("#mask").addClass("mask-on");
        },1);
      }
    }
    else {
      $("#mask").removeClass("mask");
      $("#mask").removeClass("mask-on");
      $("#mask").addClass("mask-off");
    }

    $( "#mask" ).click(function() {
      $("#menu-principal").removeClass("change-menu-principal-ativo");

      $("#mask").removeClass("mask");
      $("#mask").removeClass("mask-on");
      $("#mask").addClass("mask-off");
      // console.log("clicou na div1 com menu aberto!!!!");
      $(".menu-principal").removeClass("change-menu-principal");
      $(".logo-toggle").removeClass("change-logo-toggle");
      $(".items-menu").removeClass("change-items-menu");
      $(".logo-menu").removeClass("change-logo-menu");
      $("#menu-btn").removeClass("change");
     });

  });



  // ALTERACAO MENU MOBILE
  $("#botao-menu-mob").click(function(){
    document.getElementById("menu-principal").classList.toggle("change-menu-principal-ativo");
    
    if ($('#menu-principal').hasClass( "change-menu-principal-ativo" )) {
      $("#mask").removeClass("mask-off");
      $("#mask").addClass("mask");
      if($("#mask").hasClass("mask")){
        // adiciona a segunda classe com delay para uma transicao mais leve
        setTimeout(function() {
          $("#mask").addClass("mask-on");
        },1);
      }
    }
    else {
      $("#mask").removeClass("mask");
      $("#mask").removeClass("mask-on");
      $("#mask").addClass("mask-off");
    }

    $( "#mask" ).click(function() {
      $("#menu-principal").removeClass("change-menu-principal-ativo");

      $("#mask").removeClass("mask");
      $("#mask").removeClass("mask-on");
      $("#mask").addClass("mask-off");
      // console.log("clicou na div1 com menu aberto!!!!");
      $(".menu-principal").removeClass("change-menu-principal");
      $(".logo-toggle").removeClass("change-logo-toggle");
      $(".items-menu").removeClass("change-items-menu");
      $(".logo-menu").removeClass("change-logo-menu");
      $("#menu-btn").removeClass("change");
  });

  });
  

  // FIM ALTERACAO MENU MOBILE
  
});