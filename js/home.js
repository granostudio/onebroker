$(document).ready(function(){

    // script para acionar animacao
    $(".div6-home").mouseenter(function(){
        $(".grafico1").addClass("graf1-ativo");
        $(".grafico1").removeClass("graf1-inativo");
        // grafico 2
        $(".grafico2").addClass("graf2-ativo");
        $(".grafico2").removeClass("graf2-inativo");
    });

    // reinicia a animacao
    // $(".div6-home").mouseleave(function(){
    //     $(".grafico1").removeClass("graf1-ativo");
    //     $(".grafico1").addClass("graf1-inativo");
    //     // grafico 2
    //     $(".grafico2").removeClass("graf2-ativo");
    //     $(".grafico2").addClass("graf2-inativo");
    // });

    //   mudanca de cor de cabecalho

    $(window).bind("scroll", function() { 
        var $div1 = $('.cabecalho-login').offset().top;
        var $div2 = $('.div2-home').offset().top;
        var $div3 = $('.div3-home').offset().top;
        var $div4 = $('.div4-home').offset().top;
        var $div5 = $('.div5-home').offset().top;
        var $div6 = $('.div6-home').offset().top;
        var $div7 = $('.div7-home').offset().top;
        var $div7Bgbranco = $('.div7-home .div-bd-branco').offset().top;
        var $div7Div2 = $('.div7-home .div2').offset().top;
        var $div7Div3 = $('.div7-home .div3').offset().top;
        var $div7Div4 = $('.div7-home .div4').offset().top;
        var $divfooter = $('footer').offset().top;
        var $divcolchete = $('.img-colchete2').offset().top;
        
        // var $sec4 = $('.bg4').offset().top; 
        // var $sec5 = $('.carousel-indicators').offset().top;   
     
        if ($(this).scrollTop() < $div2){ 
          console.log("mudou de cor 1");
          $( ".cabecalho-login .cabecalho-entrar" ).css({"color":"#fff"});
        }     
        if ($(this).scrollTop() > $div2 & $(this).scrollTop() < $div3){ 
            console.log("mudou de cor 2");
            $( ".cabecalho-login .cabecalho-entrar" ).css({"color":"#000"});
        } 

        if ($(this).scrollTop() > $div3 & $(this).scrollTop() < $div4){ 
            console.log("mudou de cor 3");   
            $( ".cabecalho-login .cabecalho-entrar" ).css({"color":"#000"});
        } 

        if ($(this).scrollTop() > $div4 & $(this).scrollTop() < $div5){ 
            console.log("mudou de cor 3");   
            $( ".cabecalho-login .cabecalho-entrar" ).css({"color":"#000"});
        }

        if ($(this).scrollTop() > $div5 & $(this).scrollTop() < $div6){  
            $( ".cabecalho-login .cabecalho-entrar" ).css({"color":"#FFF"});
        } 

        if ($(this).scrollTop() >= $divcolchete & $(this).scrollTop() < $div6){    
            $( ".cabecalho-login .cabecalho-entrar" ).css({"color":"#000"});
        } 

        if ($(this).scrollTop() >= $div6 & $(this).scrollTop() < $div7){   
            $( ".cabecalho-login .cabecalho-entrar" ).css({"color":"#fff"});
        } 

        if ($(this).scrollTop() >= $div7 & $(this).scrollTop() < $divfooter){ 
            $( ".cabecalho-login .cabecalho-entrar" ).css({"color":"#fff"});
        } 

        if ($(this).scrollTop() >= $div7Bgbranco & $(this).scrollTop() < $divfooter){ 
            $( ".cabecalho-login .cabecalho-entrar" ).css({"color":"#000"});
        } 

        if ($(this).scrollTop() >= $div7Div2 & $(this).scrollTop() < $divfooter){ 
            $( ".cabecalho-login .cabecalho-entrar" ).css({"color":"#fff"});
        } 

        if ($(this).scrollTop() >= $div7Div3 & $(this).scrollTop() < $divfooter){ 
            $( ".cabecalho-login .cabecalho-entrar" ).css({"color":"#000"});
        } 

        if ($(this).scrollTop() >= $div7Div4 & $(this).scrollTop() < $divfooter){ 
            $( ".cabecalho-login .cabecalho-entrar" ).css({"color":"#000"});
        } 

        if ($(this).scrollTop() > $divfooter){ 
            $( ".cabecalho-login .cabecalho-entrar" ).css({"color":"#000"});
        }

        
        // var scrollThis = $(this).scrollTop();

        // console.log("ScrollThis: "+scrollThis);
              
      });
   
});