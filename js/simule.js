var currentTab = 0; // Current tab is set to be the first tab (0)
showTab(currentTab); // Display the current tab

function showTab(n) {
  // This function will display the specified tab of the form ...
  var x = document.getElementsByClassName("tab");
  x[n].style.display = "block";

  // ... and fix the Previous/Next buttons:
  if (n == 0) {
    document.getElementById("prevBtn").style.display = "none";
  } else {
    document.getElementById("prevBtn").style.display = "inline";
  }

  if (n == 4) {
    console.log("Chegou no grafico img!!");
    setTimeout(function(){
      $('#grafico-conteudo').removeClass('inativo');
      $('#grafico-conteudo').addClass('ativo');
      $('.circ-grafico').removeClass('circ-inativo');
      $('.circ-grafico').addClass('circ-ativo');
    }, 500);

    setTimeout(function(){
      $('.graf-amigos-full').hide();
      $('.guard-total-conteudo').show();
      $("#nextBtn").addClass("btn-final");

      setTimeout(function(){
        $('.odometer').html(123456);
        }, 1000);
    }, 4000);
 
      

  } 
  if (n == (x.length - 1)) {
    document.getElementById("nextBtn").innerHTML = "Submit";
    $("#nextBtn").removeClass("btn-final");
  }
  
  else {
    document.getElementById("nextBtn").innerHTML = "continuar";
    $("#nextBtn").removeClass("btn-final");
  }
  // ... and run a function that displays the correct step indicator:
  fixStepIndicator(n)
}

function nextPrev(n) {
  // This function will figure out which tab to display
  var x = document.getElementsByClassName("tab");
  // Exit the function if any field in the current tab is invalid:
  if (n == 1 && !validateForm()) return false;
  // Hide the current tab:
  x[currentTab].style.display = "none";
  // Increase or decrease the current tab by 1:
  currentTab = currentTab + n;
  // if you have reached the end of the form... :
  if (currentTab >= x.length) {
    //...the form gets submitted:
    document.getElementById("regForm").submit();
    return false;
  }
  // Otherwise, display the correct tab:
  showTab(currentTab);
}

function validateMail(email) {

  var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
  //var address = document.getElementById[email].value;
  if (reg.test(email) == false) 
  {
      alert('Invalid Email Address');
      return (false);
      
  }
}

function validateForm() {
  // This function deals with validation of the form fields
  var x, y, i, valid = true;
  x = document.getElementsByClassName("tab");
  y = x[currentTab].getElementsByTagName("input");
  // A loop that checks every input field in the current tab:
  for (i = 0; i < y.length; i++) {
    // If a field is empty...
    if (y[i].value == "") {
      // add an "invalid" class to the field:
      y[i].className += " invalid";
      y[i].placeholder = "Valor inválido";
      // and set the current valid status to false:
      valid = false;
    }

    if (y[i].type == 'email') {
     
      var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
  //var address = document.getElementById[email].value;
      if (reg.test(y[i].value) == false) 
      {
        y[i].className += " invalid";
        y[i].placeholder = "E-mail inválido";
        y[i].value = " ";
        valid = false;
      }
      
    }

  }
  // If the valid status is true, mark the step as finished and valid:
  if (valid) {
    document.getElementsByClassName("step")[currentTab].className += " finish";
  }
  return valid; // return the valid status
}

function fixStepIndicator(n) {
  // This function removes the "active" class of all steps...
  var i, x = document.getElementsByClassName("step");
  for (i = 0; i < x.length; i++) {
    x[i].className = x[i].className.replace(" active", "");
  }
  //... and adds the "active" class to the current step:
  x[n].className += " active";
}

// MASCARAS

function mascara(i,t){
   
  var v = i.value;
  
  if(isNaN(v[v.length-1])){
     i.value = v.substring(0, v.length-1);
     return;
  }
  
  if(t == "data"){
     i.setAttribute("maxlength", "10");
     if (v.length == 2 || v.length == 5) i.value += "/";
  }

  if(t == "cpf"){
     i.setAttribute("maxlength", "14");
     if (v.length == 3 || v.length == 7) i.value += ".";
     if (v.length == 11) i.value += "-";
  }

  if(t == "cnpj"){
     i.setAttribute("maxlength", "18");
     if (v.length == 2 || v.length == 6) i.value += ".";
     if (v.length == 10) i.value += "/";
     if (v.length == 15) i.value += "-";
  }

  if(t == "cep"){
     i.setAttribute("maxlength", "9");
     if (v.length == 5) i.value += "-";
  }

  if(t == "cel"){
     if(v[0] == 9){
        i.setAttribute("maxlength", "10"); 
        if (v.length == 5) i.value += "-";
     }else{
        i.setAttribute("maxlength", "13");
        if (v.length == 2) i.value += " ";
        if (v.length == 8) i.value += " ";
     }
  }

  if(t == "tel"){
    if(v[0] == 9){
       i.setAttribute("maxlength", "10"); 
       if (v.length == 5) i.value += "-";
    }else{
       i.setAttribute("maxlength", "12");
       if (v.length == 2) i.value += " ";
       if (v.length == 7) i.value += " ";
    }
 }

 if(t == "card"){
  if(v[0] == 9){
     i.setAttribute("maxlength", "10"); 
     if (v.length == 5) i.value += "-";
  }else{
     i.setAttribute("maxlength", "19");
     if (v.length == 4) i.value += " ";
     if (v.length == 8) i.value += " ";
     if (v.length == 12) i.value += " ";
  }
}
}

// FIM MASCARAS

  // $('.btn-exp').on('click', function() {  
  //   $('.item-principal').children('.item-secundario').toggle();
  //   console.log('click do btn');
  // });

  // $('.btn-exp').click(function(){
  //   $(this).parent($('.sub-item')).toggle();
  // });

  function acionaBtn(x, y) {
    
    if ($(x).hasClass('ativo')) {
      $(x).text('+');
      $(x).toggleClass('ativo');
    }
    else  {
      $(x).text('-');
      $(x).toggleClass('ativo');
    }
    // ESCONDE O BLOCO SECUNDARIO
    // a contagem de itens parte de 0 entao fiz a formula de -1 para o primeiro item poder ser declarado como 1
    $(x).parent($('.sub-item').eq(y - 1).toggle());
    
  }




